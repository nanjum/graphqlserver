package com.example.graphqlServer.repository;

import com.example.graphqlServer.entity.Voter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoterRepository extends CrudRepository<Voter, Integer> {
}
