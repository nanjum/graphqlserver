package com.example.graphqlServer.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example.graphqlServer.entity.Area;
import com.example.graphqlServer.repository.AreaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Transactional
@RequiredArgsConstructor
public class AreaMutationResolver implements GraphQLMutationResolver {

    private final AreaRepository areaRepository;

    public Area createArea(String name, String description) {
        Area area = new Area();
        area.setName(name);
        area.setDescription(description);
        return areaRepository.save(area);
    }
}
