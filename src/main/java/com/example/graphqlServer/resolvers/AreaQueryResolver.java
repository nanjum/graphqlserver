package com.example.graphqlServer.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.graphqlServer.entity.Area;
import com.example.graphqlServer.repository.AreaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AreaQueryResolver implements GraphQLQueryResolver {
    private final AreaRepository areaRepository;

    public Iterable<Area> getAreas() {
        return areaRepository.findAll();
    }

    public Optional<Area> getAreaById(Integer id) {
        return areaRepository.findById(id);
    }
}
