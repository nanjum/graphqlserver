package com.example.graphqlServer.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example.graphqlServer.entity.Voter;
import com.example.graphqlServer.repository.VoterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Transactional
@RequiredArgsConstructor
public class VoterMutationResolver implements GraphQLMutationResolver {
    private final VoterRepository voterRepository;

    public Voter createVoter(String nid, String name) {
        Voter voter = new Voter();
        voter.setName(name);
        voter.setNid(nid);
        return voterRepository.save(voter);
    }
}
