package com.example.graphqlServer.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.graphqlServer.entity.Voter;
import com.example.graphqlServer.repository.VoterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class VoterQueryResolver implements GraphQLQueryResolver{

    private final VoterRepository voterRepository;

    public Iterable<Voter> getVoters() {
        return voterRepository.findAll();
    }

    public Optional<Voter> getVoterById(Integer id) {
        return voterRepository.findById(id);
    }
}
